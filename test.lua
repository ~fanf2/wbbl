require "lpeg"
require "re"

local p = re.compile

local Slow = m.P{ "Scan",

        TopBit = m.R "\128\255",

        WS = m.S " \t\r\n",
        SS = m.V "WS" ^1,

        Name      = p[[ NameStart NameChar* ]],
        NameChar  = p[[ NameStart / [0-9.-] ]],
        NameStart = p[[ [A-Za-z:_] / TopBit ]],

	Scan = p[[ ( Name / SS / . )* !. ]]
}

Slow:print()

local WS = m.S " \t\r\n"
local SS = WS^1

local NameStart = m.R("\128\255", "AZ", "az") + m.S":_"
local NameChar = NameStart + m.R"09" + m.S".-"
local Name = NameStart * NameChar^0

local Fast = ( Name + SS + m.P(1) )^0 * m.P(-1)

Fast:print()
