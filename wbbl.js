// wbbl.js - Web Browser Blue Lines
// This was written by Tony Finch (dot@dotat.at)
// You may do anything with it, at your own risk.
// http://creativecommons.org/publicdomain/zero/1.0/

////////////////////////////////////////////////////////////////////////

// Parse place notation roughly according to the syntax of
// the CCCBR XML methods collection.
//
// Returns an error message or an array of changes.
//
var parse = (function () {

  // define bell units allowing stages up to 33
  // (no extended bell-unit notation)
  var bells = "1234567890ETABCDFGHJKLMNPQRSUVWYZ";
  // positions count from 0
  var bell_of_pos = bells.split("");
  var pos_of_bell;
  for (var i = 0; i < bell_of_pos.length; i++)
    pos_of_bell[bell_of_pos[i]] = i;

  // split one change from rest of place notation
  // comma marks end of palindromic block
  // allow X as well as - for cross-over changes
  // optional dot separates changes
  var re_change_rest = new RegExp("^([,]|[X-]|[" + bells + "]*)[.]?(.*)");

  function reflect(changes,start) {
    var j = changes.length - 1;
    while (j > start)
      changes.push(changes[--j]);
  }

  function change_from_places(stage,places) {
    var place = places.split("");
    for (var i = 0; i < place.length; i++)
      place[i] = pos_of_bell[place[i]];
    place.push(stage); // sentinel
    place.sort(function (a,b) { return a - b });
    var change = [];
    for (var i = 0, pos = 0; pos < stage; pos++)
      if (place[i] == pos) change[pos] = pos, i++;
      else change[pos] = pos - (place[i] - pos) % 2 * 2 + 1;
    if (change[0] < 0) change[0] = 0;
    return change;
  }

  return function (stage,pn) {
    var changes = [];
    var palindrome_start = 0;
    if (pn.length == 0)
      return "empty place notation";
    pn = pn.toUpperCase();
    while (pn.length) {
      var m = pn.match(re_change_rest);
      if (m == null || m[1] == "")
	return "syntax error in place notation at "+pn;
      var places = m[1];
      pn = m[2];
      if (places == ",") {
	reflect(changes,palindrome_start);
	palindrome_start = changes.length;
	continue;
      }
      if (places == "-" || places == "X")
	places = "";
      changes.push(change_from_places(stage,places));
    }
    if (palindrome_start > 0)
      reflect(changes,palindrome_start);
    return changes;
  }

})();

////////////////////////////////////////////////////////////////////////

function rounds(stage) {
 var row = [];
 for (var i = 0; i < stage; i++)
  row[i] = i;
 return row;
}

function draw_line(stage, changes) {
 var row = rounds(stage);
 for (var j = 0; j < changes.length; j++) {
  var change = changes[j];
  var next = [];
  for (var i = 0; i < stage; i++)
   next[change[i]] = row[i];
  row = next;
 }
 var hunters = 0;
 while (row[hunters] == hunters)
  hunters++;
 var course = changes.length * (stage - hunters);
 var lead_end = changes[changes.length - 1];
 // position of the blue line
 var bell = stage - 1;
 while (bell > 0 && lead_end[bell] != bell)
  bell--;
 // position of the red lines
 var hunter = rounds();

 var canvas = document.getElementById("canvas");

 // dimensions of a cross
 var width = canvas.width / (stage + 1);
 var height = width / 6;
 canvas.height = height * (course + 4);

 var ctx = canvas.getContext("2d");
 ctx.save();

 ctx.translate(width, height*2);

 // draw grid
 ctx.strokeStyle = "rgb(200,200,200)";
 ctx.beginPath();
 for (var i = 0; i < stage; i++) {
  ctx.moveTo(i * width, 0);
  ctx.lineTo(i * width, course * height);
 }
 ctx.stroke();

 // draw blue line
 ctx.fillStyle = "rgb(0,0,200)";
 ctx.strokeStyle = "rgb(0,0,200)";
 ctx.lineWidth = 2;
 for (var j = 0, k = 1; j < course; j = k) {
  k = j + 1;
  if (j % changes.length == 0) {
   ctx.beginPath();
   ctx.arc(bell * width, j * height, 3,
     0, 2*Math.PI, true);
   ctx.fill();
  }
  ctx.beginPath();
  ctx.moveTo(bell * width, j * height);
  bell = changes[j % changes.length][bell];
  ctx.lineTo(bell * width, k * height);
  ctx.stroke();
 }
 ctx.beginPath();
 ctx.arc(bell * width, course * height, 3,
   0, 2*Math.PI, true);
 ctx.fill();

 ctx.restore();
}

////////////////////////////////////////////////////////////////////////

var stages = [,,,
  "Singles","Minimus",
  "Doubles","Minor",
  "Triples","Major",
  "Caters","Royal",
  "Cinques","Maximus",
  "Sextuples","Fourteen",
  "Septuples","Sixteen",
  "Octuples","Eighteen",
 ];

function get_stage(title) {
 for (var i = 3; i <= stages.length; i++)
  if (title.indexOf(stages[i]) != -1)
   return i;
}

function draw_method(method) {
 var title = method.getAttribute("title");
 var stage = get_stage(title);
 var pn = method.textContent;
 var changes = parse(stage, pn);
 if (typeof(changes) == "string")
  alert(changes+" (this should not happen)");
 else
  draw_line(stage,changes);
}

////////////////////////////////////////////////////////////////////////

var tx_from = "ABCDEFGHIJKLMNOPQRSTUVWXYZÉáåčèéêöøûůṟ²₃₁";
var tx_to   = "abcdefghijklmnopqrstuvwxyzeaaceeeoouur213";
var tx_title = "translate(@title,'"+tx_from+"','"+tx_to+"')";
var start_of_test = "contains("+tx_title+",'";

function find_methods(search) {
 var match = search.split(/ +/).map(function (kw) {
    return start_of_test + kw.toLowerCase() + "')";
   }).join(" and ");
 var result = document.evaluate("//div[@id='methods']/span["+match+"]",
   document, null, 0, null);
 var method = result.iterateNext();
 var found = [];
 while (method) {
  found.push(method);
  method = result.iterateNext();
 }
 return found;
}

function clear_results() {
 var results = document.form.results;
 while (results.length)
  results.remove(0);
}

function add_result(title) {
  var option = document.createElement("option");
  option.textContent = title;
  document.form.results.add(option);
}

function bad_result(n) {
  add_result("(" + n + " methods found)");
}

function search() {
 var keywords = document.form.name.value;
 var type = document.form.type.value;
 if (type != "(any type)")
  keywords += " " + type;
 var stage = document.form.stage.value;
 if (stage != "(any stage)")
  keywords += " " + stage;
 var methods = find_methods(keywords);
 clear_results();
 if (methods.length == 0) {
  stage = get_stage(stage);
  var pn = name;
  var changes = parse(stage,pn);
  if (typeof(changes) == "string")
   return bad_result(methods.length);
  else
   add_result("(place notation)");
  draw_line(stage, changes);
  return;
 }
 if (methods.length > 100)
  return bad_result(methods.length);
 if (methods.length != 1)
  add_result("Choose...");
 methods.forEach(function (m) {
    add_result(m.getAttribute("title"));
   });
 if (methods.length == 1)
  draw_method(methods[0]);
}

function select() {
 var name = document.form.results.value;
 if (name.indexOf("'") < 0)
  name = "'"+name+"'";
 else
  name = '"'+name+'"';
 var result = document.evaluate("//div[@id='methods']/span[@title="+name+"]",
   document, null, 0, null);
 var method = result.iterateNext();
 draw_method(method);
}

// end
