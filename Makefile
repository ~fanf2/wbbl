# Makefile for wbbl - Web Browser Blue Lines

all: wbbl.html.gz index.html

wbbl.html.gz: wbbl.html
	gzip -c9 <wbbl.html >wbbl.html.gz

wbbl.html: wbbl.in wbbl.js methods.html
	rm -f wbbl.html
	sed '1,/<script/!d' <wbbl.in >>wbbl.html
	cat wbbl.js >>wbbl.html
	sed '/<\/script/,/METHODS START/!d' <wbbl.in >>wbbl.html
	cat methods.html >>wbbl.html
	sed '/METHODS END/,$$!d' <wbbl.in >>wbbl.html

methods.js methods.html: methods.xml mxstrip.lua
	lua mxstrip.lua

methods.xml: methods.mk
	make -f methods.mk

methods.mk: methods.mk.sh
	sh methods.mk.sh

index.html: intro.in
	git --no-pager log --pretty=format:'sed "s/LONG/%H/;s/SHORT/%h/;s/DATE/%ai/" <intro.in >index.html' HEAD^..HEAD | sh

copy: mxstrip.lua wbbl.html.gz
	scp	index.html \
		mxstrip.lua \
		wbbl.html.gz \
		chiark:public-html/wbbl

clean:
	rm -f methods.html methods.js methods.xml methods.mk
	rm -f wbbl.html wbbl.html.gz index.html

realclean: clean
	rm -rf methods methods.pdf *~

up:
	git gc
	git update-server-info
	touch .git/git-daemon-export-ok
	echo "Web Browser Blue Lines" >.git/description
	rsync --delete --recursive --links .git/ chiark:public-git/wbbl.git/
